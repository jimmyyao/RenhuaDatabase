package com.renhua.database;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class RenDaoGenerator {
	public static int DB_VERSION = 56; // update 2014/11/18

	public static void main(String[] args) throws Exception {
		Schema schema = new Schema(DB_VERSION, "com.renhua.database");
		addCharts(schema);
		addPlazaMsg(schema);
		addFriendAchieve(schema);
		addFriends(schema);
		addContracts(schema);
		addPaymentAccount(schema);
		addNaviMsg(schema);
		addCollectionsInfo(schema);
		addWinwhoUsers(schema);
		addWindowWelfae(schema);
		addCoDonate(schema);
		addCoDonateDetailImg(schema);
		addWinCoinIncomeHistory(schema);
		addDonateHistory(schema);
		addConvertHistory(schema);
		addMyEfforts(schema);
		addImg(schema);
		addAdv(schema);
		addAdv2(schema);
		addCategory(schema);
		addDownloadInfo(schema);
		addRecommendAdvPojo(schema);
		addAssnDonate(schema);
		addWelfare(schema);
		addDownloadApk(schema);
		addMyDonate(schema);
		addDonateTop(schema);
		addGroupTop(schema);
		new DaoGenerator().generateAll(schema, "./src-gen");
	}

	private static void addDonateTop(Schema schema) {
		Entity user = schema.addEntity("donateTop");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addLongProperty("donate_members");
		user.addStringProperty("title");
		user.addStringProperty("sub_title");
		user.addStringProperty("title_img");
		user.addIntProperty("sort_id");
	}

	private static void addGroupTop(Schema schema) {
		Entity user = schema.addEntity("groupTop");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addLongProperty("donate_members");
		user.addStringProperty("title");
		user.addStringProperty("sub_title");
		user.addStringProperty("title_img");
		user.addIntProperty("sort_id");
	}

	private static void addMyDonate(Schema schema) {
		Entity my = schema.addEntity("MyDonate");
		my.addLongProperty("identify").primaryKey();
		my.addLongProperty("id");
		my.addStringProperty("title");
		my.addStringProperty("title_img");
		my.addLongProperty("donate_members");
		my.addIntProperty("type");
	}

	private static void addWelfare(Schema schema) {
		Entity wel = schema.addEntity("Welfare");
		wel.addIdProperty().primaryKey();
		wel.addStringProperty("title");
		wel.addStringProperty("reward");
		wel.addStringProperty("image");
		wel.addStringProperty("content");
		wel.addIntProperty("sort_id");
		wel.addIntProperty("type");
		wel.addStringProperty("packageName");
		wel.addIntProperty("isInstall");
	}

	private static void addDownloadApk(Schema schema) {
		Entity downloadApk = schema.addEntity("downloadApk");
		downloadApk.addIdProperty();
		downloadApk.addBooleanProperty("isFinish");
		downloadApk.addStringProperty("downloadPath");
		downloadApk.addIntProperty("fileLength");
		downloadApk.addStringProperty("url").primaryKey();
	}

	private static void addRecommendAdvPojo(Schema schema) {
		Entity adv = schema.addEntity("RecommendAdvPojoDataBase");
		adv.addIdProperty().primaryKey();
		adv.addIntProperty("seq");
		adv.addLongProperty("advId");

	}

	private static void addDownloadInfo(Schema schema) {
		Entity download = schema.addEntity("Download");
		download.addStringProperty("downPath");
		download.addIntProperty("downLength");
		download.addIntProperty("threadId");
	}

	private static void addCategory(Schema schema) {
		Entity category = schema.addEntity("Category");
		category.addIdProperty().primaryKey();
		category.addStringProperty("name");
		category.addIntProperty("seq");
		category.addStringProperty("icon");
		category.addIntProperty("isChoose");
	}

	private static void addAdv(Schema schema) {
		Entity adv = schema.addEntity("AdvDataBase");
		adv.addIdProperty().primaryKey();
		adv.addStringProperty("title");
		adv.addStringProperty("content");
		adv.addStringProperty("content_image");
		adv.addLongProperty("biz_window_id");
		adv.addLongProperty("adv_owner_id");
		adv.addStringProperty("adv_owner_name");
		adv.addStringProperty("websiteMap");
		adv.addStringProperty("phoneMap");
		adv.addIntProperty("weight");
		adv.addLongProperty("start_time");
		adv.addLongProperty("end_time");
		adv.addStringProperty("url");
		adv.addIntProperty("seq");
		adv.addLongProperty("sequence");
		adv.addIntProperty("clock_position");
		adv.addIntProperty("clock_color");
		adv.addIntProperty("showCount");
		adv.addStringProperty("preview");
		adv.addIntProperty("download_count");
		adv.addStringProperty("tag");
		adv.addIntProperty("interactive_mode");
		adv.addLongProperty("users");
		adv.addLongProperty("award");
		adv.addStringProperty("award_condition");
		adv.addBooleanProperty("bSelected");
		adv.addStringProperty("luckyPojo");
		adv.addIntProperty("is_enabled");
		adv.addStringProperty("biz_window_ids");
		adv.addIntProperty("state");
		adv.addStringProperty("owner_name");
		adv.addLongProperty("owner_id");
		adv.addStringProperty("description");
		adv.addLongProperty("update_time");
	}

	private static void addAdv2(Schema schema) {
		Entity adv = schema.addEntity("AdvDataBase2");
		adv.addIdProperty().primaryKey();
		adv.addStringProperty("title");
		adv.addStringProperty("content");
		adv.addStringProperty("content_image");
		adv.addLongProperty("biz_window_id");
		adv.addLongProperty("adv_owner_id");
		adv.addStringProperty("adv_owner_name");
		adv.addStringProperty("websiteMap");
		adv.addStringProperty("phoneMap");
		adv.addIntProperty("weight");
		adv.addLongProperty("start_time");
		adv.addLongProperty("end_time");
		adv.addStringProperty("url");
		adv.addIntProperty("seq");
		adv.addLongProperty("sequence");
		adv.addIntProperty("clock_position");
		adv.addIntProperty("clock_color");
		adv.addIntProperty("showCount");
		adv.addStringProperty("preview");
		adv.addIntProperty("download_count");
		adv.addStringProperty("tag");
		adv.addIntProperty("interactive_mode");
		adv.addLongProperty("users");
		adv.addLongProperty("award");
		adv.addStringProperty("award_condition");
		adv.addBooleanProperty("bSelected");
		adv.addStringProperty("luckyPojo");
		adv.addIntProperty("is_enabled");
		adv.addStringProperty("biz_window_ids");
		adv.addIntProperty("state");
		adv.addStringProperty("owner_name");
		adv.addLongProperty("owner_id");
		adv.addStringProperty("description");
		adv.addLongProperty("update_time");
	}

	private static void addImg(Schema schema) {
		Entity imgs = schema.addEntity("cacheImgs");
		imgs.addStringProperty("imgname").primaryKey();
		imgs.addStringProperty("imgcontent");

	}

	private static void addPlazaMsg(Schema schema) {
		Entity plazaMsg = schema.addEntity("plazaMsg");
		plazaMsg.addIdProperty().primaryKey();
		plazaMsg.addStringProperty("uid");
		plazaMsg.addStringProperty("uid_nick_name");
		plazaMsg.addStringProperty("uid_logo");
		plazaMsg.addStringProperty("fid");
		plazaMsg.addStringProperty("fid_nick_name");
		plazaMsg.addStringProperty("fid_logo");
		plazaMsg.addStringProperty("content");
		plazaMsg.addStringProperty("title");
		plazaMsg.addIntProperty("msgType");
		plazaMsg.addLongProperty("msgTime");
		plazaMsg.addBooleanProperty("isRemove");
	}

	private static void addFriendAchieve(Schema schema) {
		Entity friendsAchieveMsg = schema.addEntity("FriendsAchieveMsg");
		friendsAchieveMsg.addIdProperty().primaryKey();
		friendsAchieveMsg.addStringProperty("uid");
		friendsAchieveMsg.addStringProperty("uid_nick_name");
		friendsAchieveMsg.addStringProperty("uid_logo");
		friendsAchieveMsg.addStringProperty("fid");
		friendsAchieveMsg.addStringProperty("fid_nick_name");
		friendsAchieveMsg.addStringProperty("fid_logo");
		friendsAchieveMsg.addStringProperty("content");
		friendsAchieveMsg.addStringProperty("title");
		friendsAchieveMsg.addIntProperty("msgType");
		friendsAchieveMsg.addLongProperty("msgTime");
		friendsAchieveMsg.addBooleanProperty("isRemove");

	}

	private static void addMyEfforts(Schema schema) {
		Entity achieve = schema.addEntity("MyEfforts");
		achieve.addIdProperty().primaryKey();
		achieve.addLongProperty("userid");
		achieve.addStringProperty("nick_name");
		achieve.addStringProperty("logo");
		achieve.addStringProperty("title");
		achieve.addLongProperty("msgTime");
		achieve.addLongProperty("praise_count");
		achieve.addStringProperty("praise_names");
	}

	private static void addConvertHistory(Schema schema) {
		Entity user = schema.addEntity("ConvertHistory");
		user.addIdProperty().primaryKey();
		user.addStringProperty("date");
		user.addStringProperty("coin");
	}

	private static void addDonateHistory(Schema schema) {
		Entity user = schema.addEntity("DonateHistory");
		user.addIdProperty().primaryKey();
		user.addStringProperty("conName");
		user.addStringProperty("content");
		user.addStringProperty("date");
		user.addStringProperty("donateWincoin");
	}

	private static void addWinCoinIncomeHistory(Schema schema) {
		Entity user = schema.addEntity("WinCoinIncomeHistory");
		user.addIdProperty().primaryKey();
		user.addStringProperty("date");
		user.addIntProperty("wincoin");
	}

	private static void addCoDonateDetailImg(Schema schema) {
		Entity user = schema.addEntity("CoDonateDetailImg");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addLongProperty("co_donate_id");
		user.addIntProperty("order");
		user.addStringProperty("img");
		user.addStringProperty("comment");
	}

	private static void addCoDonate(Schema schema) {
		Entity user = schema.addEntity("CoDonate");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addLongProperty("donate_members");
		user.addStringProperty("title");
		user.addStringProperty("sub_title");
		user.addStringProperty("title_img");
		user.addIntProperty("sort_id");
	}

	private static void addAssnDonate(Schema schema) {
		Entity user = schema.addEntity("AssnDonate");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addLongProperty("donate_members");
		user.addStringProperty("title");
		user.addStringProperty("sub_title");
		user.addStringProperty("title_img");
		user.addIntProperty("sort_id");
	}

	private static void addWindowWelfae(Schema schema) {
		Entity user = schema.addEntity("WindowCoEntity");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addLongProperty("commonweal_id");
		user.addStringProperty("title");
		user.addStringProperty("sub_title");
		user.addStringProperty("description");
		user.addStringProperty("logo");
		user.addIntProperty("seq");
		user.addLongProperty("create_time");
	}

	private static void addWinwhoUsers(Schema schema) {
		Entity user = schema.addEntity("Users");
		user.addIdProperty().primaryKey();
		user.addLongProperty("uid");
		user.addStringProperty("passwd");
		user.addStringProperty("phone");
		user.addBooleanProperty("online");
		user.addStringProperty("nickname");
		user.addStringProperty("gusturePattern");
		user.addStringProperty("numberPattern");
		user.addBooleanProperty("lockScreenEnable");
		user.addBooleanProperty("statusBarEnable");
		user.addBooleanProperty("pushEnable");
		user.addBooleanProperty("onlyWifiEnable");
		user.addStringProperty("photopath");
		user.addBooleanProperty("isGuest");
	}

	private static void addCollectionsInfo(Schema schema) {
		Entity collectionInfo = schema.addEntity("CollectionInfo");
		collectionInfo.addLongProperty("id");
		collectionInfo.addLongProperty("uid");
		collectionInfo.addLongProperty("advId").primaryKey();
		collectionInfo.addLongProperty("favTime");
		collectionInfo.addIntProperty("imgSeq");
		collectionInfo.addIntProperty("isRemove");
		collectionInfo.addStringProperty("title");
		collectionInfo.addLongProperty("endTime");
		collectionInfo.addBooleanProperty("isEnabled");
		collectionInfo.addStringProperty("cnname");
		collectionInfo.addStringProperty("enname");
		collectionInfo.addStringProperty("imgUrl");
		collectionInfo.addBooleanProperty("saved");
		collectionInfo.addLongProperty("lastSysTime");
	}

	/*
	 * private static void addBrandWndInfo(Schema schema){ Entity advWndInfo = schema.addEntity("BrandWndInfo"); advWndInfo.addIdProperty().primaryKey();
	 * advWndInfo.addStringProperty("wndType"); advWndInfo.addStringProperty("wndTitle"); advWndInfo.addStringProperty("wndNotes"); advWndInfo.addStringProperty("content"); } private
	 * static void addCharityWndInfo(Schema schema){ Entity advWndInfo = schema.addEntity("CharityWndInfo"); advWndInfo.addIdProperty().primaryKey();
	 * advWndInfo.addStringProperty("wndType"); advWndInfo.addStringProperty("wndTitle"); advWndInfo.addStringProperty("wndNotes"); advWndInfo.addStringProperty("content"); }
	 */

	private static void addCharts(Schema schema) {
		Entity charts = schema.addEntity("Charts");
		charts.addIdProperty().primaryKey();
		charts.addStringProperty("uid");
		charts.addStringProperty("uid_nick_name");
		charts.addStringProperty("uid_logo");
		charts.addStringProperty("fid");
		charts.addStringProperty("fid_nick_name");
		charts.addStringProperty("fid_logo");
		charts.addStringProperty("content");
		charts.addStringProperty("title");
		charts.addIntProperty("msgType");
		charts.addLongProperty("msgTime");
		charts.addBooleanProperty("isRemove");
	}

	private static void addFriends(Schema schema) {
		Entity friends = schema.addEntity("Friends");
		friends.addIdProperty().primaryKey();
		friends.addStringProperty("uid");
		friends.addStringProperty("fid");
		friends.addStringProperty("avatar");
		friends.addStringProperty("applyTime");
		friends.addStringProperty("agreeTime");
		friends.addStringProperty("removeTime");
		friends.addIntProperty("replyStats");
		friends.addIntProperty("robbed");
		friends.addStringProperty("nickname");
		friends.addStringProperty("wincoin");
		friends.addStringProperty("ordername");
		friends.addLongProperty("mobile");
	}

	private static void addContracts(Schema schema) {
		Entity contracts = schema.addEntity("Contracts");
		contracts.addIdProperty().primaryKey();
		contracts.addStringProperty("uid");
		contracts.addStringProperty("fid");
		contracts.addStringProperty("fname");
		contracts.addStringProperty("phone");
		contracts.addIntProperty("is_friend");
		contracts.addIntProperty("is_member");
	}

	private static void addPaymentAccount(Schema schema) {
		Entity payments = schema.addEntity("PaymentAccount");
		payments.addIdProperty().primaryKey();
		payments.addStringProperty("trueName");
		payments.addStringProperty("personId");
		payments.addStringProperty("bankName");
		payments.addStringProperty("accountId");
		payments.addStringProperty("accountType");
		payments.addIntProperty("isDefault");
	}

	private static void addNaviMsg(Schema schema) {
		Entity naviMsg = schema.addEntity("NaviMsg");
		naviMsg.addIdProperty().primaryKey();
		naviMsg.addStringProperty("uid");
		naviMsg.addStringProperty("uid_nick_name");
		naviMsg.addStringProperty("uid_logo");
		naviMsg.addStringProperty("fid");
		naviMsg.addStringProperty("fid_nick_name");
		naviMsg.addStringProperty("fid_logo");
		naviMsg.addStringProperty("content");
		naviMsg.addStringProperty("title");
		naviMsg.addIntProperty("msgType");
		naviMsg.addLongProperty("msgTime");
		naviMsg.addBooleanProperty("isRemove");
	}

	/*
	 * private static void addNote(Schema schema) { Entity note = schema.addEntity("Note"); note.addIdProperty(); note.addStringProperty("text").notNull();
	 * note.addStringProperty("comment"); note.addDateProperty("date"); }
	 * 
	 * private static void addCustomerOrder(Schema schema) { Entity customer = schema.addEntity("Customer"); customer.addIdProperty(); customer.addStringProperty("name").notNull();
	 * 
	 * Entity order = schema.addEntity("Order"); order.setTableName("ORDERS"); // "ORDER" is a reserved keyword order.addIdProperty(); Property orderDate =
	 * order.addDateProperty("date").getProperty(); Property customerId = order.addLongProperty("customerId").notNull().getProperty(); order.addToOne(customer, customerId);
	 * 
	 * ToMany customerToOrders = customer.addToMany(order, customerId); customerToOrders.setName("orders"); customerToOrders.orderAsc(orderDate); }
	 */

}

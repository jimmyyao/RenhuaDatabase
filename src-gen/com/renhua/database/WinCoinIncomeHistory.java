package com.renhua.database;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table WIN_COIN_INCOME_HISTORY.
 */
public class WinCoinIncomeHistory {

    private Long id;
    private String date;
    private Integer wincoin;

    public WinCoinIncomeHistory() {
    }

    public WinCoinIncomeHistory(Long id) {
        this.id = id;
    }

    public WinCoinIncomeHistory(Long id, String date, Integer wincoin) {
        this.id = id;
        this.date = date;
        this.wincoin = wincoin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getWincoin() {
        return wincoin;
    }

    public void setWincoin(Integer wincoin) {
        this.wincoin = wincoin;
    }

}
